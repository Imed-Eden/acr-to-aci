import json
import os
from .azure_sdk import AzureSDK
from azure.graphrbac import GraphRbacManagementClient
from azure.graphrbac.operations import GroupsOperations
from azure.common.credentials import get_azure_cli_credentials


class Aci(AzureSDK):
    def __init__(self, creds, project_name, subscription, image_id, location):
        super(Aci, self).__init__(creds, project_name, subscription, image_id, location)
        
    def create(self):
        return self.create_aci()

    def list(self):
        containers = self.list_container_groups()
        for container in containers :
            return container
    def delete(self):
        return self.delete_container_group()
